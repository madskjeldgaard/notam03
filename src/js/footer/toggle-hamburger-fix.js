/*
 * Toggle the .is-active class on the hamburger menu to make Jonsuh's
 * css-animated hamburger menus work with this theme. 
 *
 * Without this, the css animations won't work. See
 * https://github.com/jonsuh/hamburgers/issues/54
 *
 * Source https://jonsuh.com/hamburgers/
 *
 */

(function($) {
  $(".hamburger").on("click", function() {
    $(this).toggleClass("is-active");
  });
})(jQuery);
