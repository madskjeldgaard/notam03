<?php
/**
 * Template part for displaying front page content in front-page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package notam03
 *
 * NOTE: Title and edit links have been removed
 *
 */
?>

<div class="entry-header-item">
    <header class="entry-header">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    </header><!-- .entry-header -->
</div> <!-- .entry-header-item -->

    <?php notam03_post_thumbnail(); ?>

    <div class="entry-content page-content-item">
        <?php
        the_content();

        wp_link_pages(array(
            'before' => '<div class="page-links">' . esc_html__('Pages:', 'notam03'),
            'after'  => '</div>',
        ));
        ?>
    </div><!-- .entry-content -->

    <?php if (!is_front_page()) {?>
</article><!-- #post-<?php the_ID(); ?> -->
<?php } ?>
