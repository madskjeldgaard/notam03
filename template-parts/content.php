<?php

/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package notam03
 */

/*

This is the content template for displaying blog posts etc.

It's laid out using css grids.

This is the container for the grid layout of the content.

*/

?>

<?php
/* Legacy stuff from old notam theme that used a lot of custom fields */
$thumbnail = '';
$body = '';

/**
 * Detect plugin. For use on Front End only.
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if ( is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ):

    $thumbnail = get_field('thumbnail');
    $body = get_field('body');

endif;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
         <header class="entry-header ">

			 <?php 			
			the_title( '<h1 class="entry-title"><a id="no-decoration-black" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
?>

			 <div class="entry-meta entry-meta-item">
				 <?php echo notam03_posted_on(); /* notam03_posted_by(); */ ?>
			 </div><!-- .entry-meta  -->

         </header><!-- .entry-header -->

         <div class="entry-content ">

            <!-- Thumbnail -->
            <?php if (!$thumbnail): ?>
                <?php notam03_post_thumbnail(); ?>
			<?php elseif ($thumbnail): ?>
				<div class="thumbnail new-thumb">
					<img src="<?php echo $thumbnail['url'] ?>" alt="">
				</div>
			<?php endif; ?>

            <!-- Content -->
            <?php if(!$body) : ?>
                <?php
                the_content(sprintf(
                    wp_kses(
                        /* translators: %s: Name of current post. Only visible to screen readers */
                        __('Continue reading<span class="screen-reader-text"> "%s"</span>', 'notam03'),
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    get_the_title()
                ));

                /* Page links */
                wp_link_pages(array(
                    'before' => '<div class="page-links">' . esc_html__('Pages:', 'notam03'),
                    'after'  => '</div>',
                ));
                ?>
            <?php else : ?>
                <div class="nyhet-body">
                    <?php echo $body; ?>
                </div>
            <?php endif; ?>

         </div><!-- .entry-content -->


        <div class="entry-content-footer">
             <footer class="entry-footer ">
                 <?php /*the_post_navigation(); */?>
                 <?php /*notam03_entry_footer();*/ ?>
             </footer><!-- .entry-footer -->
       </div><!-- .entry-content-footer -->


</article><!-- #post-<?php the_ID(); ?> -->
