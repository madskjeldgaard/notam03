<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package notam03
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php the_title( sprintf( '<h3 class="entry-title"><a id="no-decoration" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php
			notam03_posted_on(); // For some reason this isn't working?
			/* notam03_posted_by(); */
			?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

    <div class="entry-content ">

	<?php notam03_post_thumbnail(); ?>

	<div class="entry-summary">
        <?php

        the_excerpt();

        ?>
	</div><!-- .entry-summary -->

</div><!-- .entry-content -->

	<footer class="entry-footer">
        <?php
            /* notam03_entry_footer(); */
?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->

