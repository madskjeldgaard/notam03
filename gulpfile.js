var gulp = require("gulp"),
  sass = require("gulp-sass"),
  browserSync = require("browser-sync"),
  concat = require("gulp-concat"),
  uglify = require("gulp-uglify"),
  del = require("del"),
  reload = browserSync.reload,
  autoprefixer = require("gulp-autoprefixer"),
  cleanCss = require("gulp-clean-css");

// Change this to something relevant for your system
// This is for using Mamp on my computer
var proxyAddr = "localhost/localnotam";

var paths = {
  // Minify and concat sources
  srcSASS: "src/scss/*.scss",
  srcJS: ["src/js/header/*.min.js", "src/js/header/*.js"],
  srcFooterJS: ["src/js/footer/*.js"],

  // Wordpress license
  license: "src/css/license.css",

  // Minify and concat destinations
  destCSS: "",
  destJS: "js",
  destJSmini: "header.js",
  destJSminifooter: "footer.js",

  // Build paths
  srcFiles: "**/*/",
  deleteFromDist: [
    "dist/.gitignore",
    "dist/node_modules",
    "dist/src",
    "dist/dist",
    "dist/gulpfile.js",
    "dist/package.json",
    "dist/package-lock.json"
  ],
  productionDir: "dist"
};

gulp.task("default", ["watch"]);

gulp.task("sass", function() {
  return gulp
    .src(paths.srcSASS)
    .pipe(sass())
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"],
        cascade: false
      })
    )
    .pipe(cleanCss({ compatibility: "ie8" }))
    .pipe(gulp.dest(paths.destCSS))
    .pipe(
      reload({
        stream: true
      })
    );
});

gulp.task("wp-license", function() {
  // return gulp
  //   .src(["src/css/license.css", "style.css"])
  //   .pipe(concat(paths.destCSS))
  //   .pipe(gulp.dest(paths.destCSS));
});

gulp.task("serve", function() {
  browserSync.init;
});

gulp.task("scripts", function() {
  return gulp
    .src(paths.srcJS)
    .pipe(concat(paths.destJSmini))
    // .pipe(uglify())
    .pipe(gulp.dest(paths.destJS))
    .pipe(
      reload({
        stream: true
      })
    );
});

gulp.task("footer-scripts", function() {
  return gulp
    .src(paths.srcFooterJS)
    .pipe(concat(paths.destJSminifooter))
    .pipe(uglify())
    .pipe(gulp.dest(paths.destJS))
    .pipe(
      reload({
        stream: true
      })
    );
});

gulp.task("browser-sync", function() {
  browserSync.init({
    proxy: proxyAddr,
    reloadOnRestart: true
  });
});

gulp.task("copy-dist", function() {
  return gulp.src(paths.srcFiles).pipe(gulp.dest(paths.productionDir));
});

gulp.task("clean", function() {
  del(paths.deleteFromDist);
});

gulp.task(
  "build",
  ["sass", "scripts", "footer-scripts", "copy-dist"],
  function() {
    del(paths.deleteFromDist);
  }
);

gulp.task("watch", ["browser-sync"], function() {
  gulp.watch(paths.srcJS, ["scripts"]);
  gulp.watch(paths.srcFooterJS, ["footer-scripts"]);
  gulp.watch(paths.srcSASS, ["sass"]);
  gulp.watch(paths.srcFooterJS, ["footer-scripts"]);
  gulp.watch(paths.srcSASS, ["sass"]);
});
