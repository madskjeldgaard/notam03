# A guide to the notam03 wordpress theme

![logo](../screenshot.png)

This guide is targeted at employees at Notam who need to update/use the website
notam.no. This is both a description of how the theme works but also a best
practice sort of thing.

### Signing in to the backend

To log in to the website, go to [notam.no/wp-admin](http://notam.no/wp-admin)
and sign in using your credentials. If you type an uncorrect password three
times in a row, you will be blocked for a while (this is a security measure).

When signed in, you will be redirected to the dashboard where you can edit the
website.

### Shortcodes

A lot of the website's functionality is boiled down to tiny pieces of code that
let's you display something. These tiny code pieces are called
[shortcodes](https://en.support.wordpress.com/shortcodes/) and they
look like this: `[this is a shortcode]`.

### Slider(s)

All image/video slider functionality is handled by the Smart Slider plugin. You
can find this plugin by signing in to the backend of the website. On the
dashboard it's located in the menu on your left, near the bottom. Click this and
you will enter a special area of the backend where you can create, update and
design sliders containing images and or video.

Making the sliders themselves is very straightforward. The plugin has a very
nice gui. When you are done, go to the "publish" part of the slider you're
editing, here you should see a small box saying "Shortcode" including a small
shortcode sort of like this: `[smartslider3 slider=2]`. Copy this code and paste
it on the page or in the post/project where you need it and it will
automatically display itself. If you go back and change the slider you made
later on, the changes will be updated to the places where you put the shortcode.

For more info on how to use this functionality: [smart slider](https://smartslider3.helpscoutdocs.com/)

### Display posts / projects anyhwere

This is one of the most important concepts in this theme. The theme was built
with custom support for the plugin [Display Posts](https://displayposts.com/).
Basically, what this does is allow us to display any kind of post / projects
wherever we want and basically how we want using a small piece of shortcode. The
shortcode begins with `display-posts` and is then followed by optional range of
parameters to help you filter what kind of stuff you want to display. [There are
many parameters to choose from, as you can see here](https://displayposts.com/docs/parameters/).

Note: Notam has a special parameter here: You can choose to add 'layout="list"'
to the shortcode parameters to display the content as a list, like so:
`[display-posts post_type="prosjekt" layout="list"]`.

#### Examples

**Show all posts categorized as "Forskning og utvikling"(note that the title of the catogory has to be written in small caps and with hyphens in stead of spaces between the words)**

`[display-posts category_name="forskning-og-utvikling"]`

**Show projects**

`[display-posts post_type="prosjekt"]`

**Show projects in category Software**

`[display-posts post_type="prosjekt" category_name="software"]`

### Categories

When making a post or creating a project, it is very important to categorize it
properly. A proper categorization means that the website's different filters
(see _Display posts / projects anyhwere_) are able to find the posts within that
category and display them together, if necessary.

When creating a post / project / page, on the right side of the editor there is
a box called Categories. Here you can see All Categories or Most Used. If what
you're writing is within one of these, mark it. If you need to add a new
category, click Add New Category (but make sure it doesn't already exist first).

You can see all the current categories and add new ones by logging in to the
dashboard, clicking on "Posts" on the left side of the screen and from the menu
that unfolds choose categories.

[More info about categories](https://www.wpbeginner.com/glossary/category/)

### Calendar

All calendar stuff is handled by the plugin The Events Calendar. Adding an entry
to the calendar is just like writing a post, page, project etc. You can find the
link to the calendar in the dashboard.

Note: Remember to use categories!

#### Page width / templates

You can choose between different page widths for each page: Full, left, slim,
wide (default). This is done in the editor of a page. On the right side, find a
box called Page Attributes and inside of that there's a dropdown box called
Template. Choose the page width you want from here

### Menu

You can now change the menu by logging in to the dashboard and clicking on
Apearrance, this will unfold a menu containing the menu item "Menus"

### Search

Search is handled by the plugin Ajax Search. You can modify this in the
dashboard in the menu on the left, at the bottom: "Ajax Search Lite"
