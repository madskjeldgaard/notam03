<?php
/**
* Template Name: Wide page
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package notam03
*/

get_header();
?>

<div id="primary" class="content-area">
<main id="main" class="site-main">
    <div class="page-grid-wide">
	<?php while (have_posts()) : the_post(); ?>
	<?php get_template_part('template-parts/content', 'page');?>
	<?php endwhile; // End of the loop. ?>
    </div><!--.page-grid -->
</main><!-- #main -->
</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
