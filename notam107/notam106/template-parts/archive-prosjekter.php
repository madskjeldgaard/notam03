<?php
/**
 * The template for displaying archive pages for the custom post type prosjekter
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package notam03
 */
?>
<?php
/* Legacy stuff from old notam theme that used a lot of custom fields */
$thumbnail = '';
$body = '';

/**
 * Detect plugin. For use on Front End only.
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if ( is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ):

    $thumbnail = get_field('thumbnail');

endif;
?>

<div class="entry-header-item">
         <header class="entry-header ">
	    <a class="animated-link" href="<?php the_permalink(); ?>"><b><?php echo get_field('ar'); ?></b></a>
	    <a id="no-decoration" href="<?php the_permalink(); ?>"><h4 class="entry-title"><?php the_title(); ?> – <?php the_field('description'); ?></h4></a>
         </header><!-- .entry-header -->
     </div><!-- .entry-header-item -->

    <div class="entry-content-item">
         <div class="entry-content ">
	    <!-- TODO: GRIDS -->
	   <div class="prosjekt-archive-grid">

		<div class="prosjekt-detail-item prosjekt-meta">
		    <!-- Details -->
		    <?php if( have_rows('details') ): ?>

			<?php while ( have_rows('details') ) : the_row(); ?>
				<div class="prosjekt-details smalltext prosjekt-<?php the_sub_field('name'); ?>">

					<b><?php the_sub_field('name'); ?>: </b>
					<span><?php the_sub_field('body'); ?></span>
				</div>
			<?php endwhile; ?>

		    <?php endif; ?>
		</div> <!-- .prosjekt-detail-item -->

		<!-- Thumbnail -->
		<?php if (!$thumbnail): ?>
		<?php notam03_post_thumbnail(); ?>
		<?php elseif ($thumbnail): ?>
			<div class="prosjekt-thumbnail-item">
			    <a id="no-decoration" href="<?php the_permalink(); ?>">
				<img src="<?php echo $thumbnail['url'] ?>" alt="">
			    </a>
			</div>
		<?php endif; ?>
		</div> <!-- .prosjekt-archive-grid -->
	    </div>
	</div><!-- .entry-content -->
     </div><!-- entry-content-item -->


        <div class="entry-content-footer">
             <footer class="entry-footer ">
                 <?php the_post_navigation(); ?>
                 <?php /*notam03_entry_footer();*/ ?>
             </footer><!-- .entry-footer -->
       </div><!-- .entry-content-footer -->


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
</article><!-- #post-<?php the_ID(); ?> -->
