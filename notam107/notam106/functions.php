<?php
/**
 * notam03 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package notam03
 */

/******************************************
*			WP STUFF
******************************************/

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

function notam03_widgets_init(){

	// These 5 widget areas are reservered for the site footer, each of them in it's own seperate cell
    register_sidebar( array(
        'name'          => __( 'Footer 1', 'textdomain' ),
        'id'            => 'footer-widgets1',
        'description'   => __( 'First part of the (desktop) footer', 'textdomain' ),
		'class' => 'footer-widget',
		'before_widget' => '<div class="footer-widget">',
		'after_widget'  => '</div>',
		'before_title' => '<span class="hidden">',
		'after_title' => '</span>',
	) );

    register_sidebar( array(
        'name'          => __( 'Footer 2', 'textdomain' ),
        'id'            => 'footer-widgets2',
        'description'   => __( 'Second part of the (desktop) footer', 'textdomain' ),
        'before_widget' => '<div class="footer-widget">',
        'after_widget'  => '</div>',
		'class' => 'footer-widget',
		'before_title' => '<span class="hidden">',
		'after_title' => '</span>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer 3', 'textdomain' ),
        'id'            => 'footer-widgets3',
        'description'   => __( 'Third part of the (desktop) footer', 'textdomain' ),
        'before_widget' => '<div class="footer-widget">',
        'after_widget'  => '</div>',
		'class' => 'footer-widget',
		'before_title' => '<span class="hidden">',
		'after_title' => '</span>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer 4', 'textdomain' ),
        'id'            => 'footer-widgets4',
        'description'   => __( 'Fourth part of the (desktop) footer', 'textdomain' ),
        'before_widget' => '<div class="footer-widget">',
        'after_widget'  => '</div>',
		'class' => 'footer-widget',
		'before_title' => '<span class="hidden">',
		'after_title' => '</span>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Footer 5', 'textdomain' ),
        'id'            => 'footer-widgets5',
        'description'   => __( 'Fifth part of the (desktop) footer', 'textdomain' ),
        'before_widget' => '<div class="footer-widget">',
        'after_widget'  => '</div>',
		'class' => 'footer-widget',
		'before_title' => '<span class="hidden">',
		'after_title' => '</span>',
    ) );

}

add_action('widgets_init', 'notam03_widgets_init');

if (! function_exists('notam03_post_thumbnail')) :
    /**
     * Displays an optional post thumbnail.
     *
     * Wraps the post thumbnail in an anchor element on index views, or a div
     * element when on single views.
     */
    function notam03_post_thumbnail()
    {
        if (post_password_required() || is_attachment() || ! has_post_thumbnail()) {
            return;
        }

        if (is_singular()) :
              /*
               * This is maybe a bit of a hack, but:
               *
               * Check to see if there is a caption set for the featured image. If there
               * is, wrap the image and the caption in a grid container (that lays it
               * out). Otherwise leave it out and let the image be full page width
               *
               */

            $thumbnailgrid = 'thumbnail-caption-grid-container';
            $get_caption = get_post(get_post_thumbnail_id())->post_excerpt;

            if (!empty($get_caption)) {
                    /*

                    Captioned thumbnail

                    */

                echo '<div class="post-thumbnail ' . $thumbnailgrid . '">';

                the_post_thumbnail(
                    null,
                    array('class' => 'thumbnail-image-item')
                );

                    echo '<div class="thumbnail-caption thumbnail-caption-item">';

                    the_post_thumbnail_caption();

                    echo '</div> <!-- .thumbnail-caption-item -->';
                    echo '</div><!-- .post-thumbnail -->';
            } else {
                    /*

                    Not captioned thumbnail

                    */

                the_post_thumbnail(
                    null,
                    array('class' => 'post-thumbnail thumbnail-no-caption-grid-container')
                );
            }
		endif;
    }
endif;

// Thumbnail size
add_theme_support('post-thumbnails');
add_image_size('square-thumb', 300, 300, true);

/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more($more)
{
	return '...';
};
add_filter('excerpt_more', 'wpdocs_excerpt_more');


// Shortcode: archivelink
function post_type_archive_link_shortcode( $atts ){

    if (array_key_exists( 'post_type', $atts )) {

	$link = get_post_type_archive_link($atts['post_type']);
	$type = $atts['post_type'];

    } elseif(array_key_exists( 'category', $atts )){

	$cat = get_category_by_slug($atts['category']);
	$link = get_category_link( $cat->term_id);
	$type = $atts['category'];

    } else{
	return '';
    };

    if (array_key_exists( 'text', $atts )){
	$text = $atts['text'];
    } else{
	$text = "Archive for ";
    };

    return "<a class='shortcode-archive-link' href=" . $link . ">Archive for " . $type . "</a>";
};

add_shortcode('archivelink', 'post_type_archive_link_shortcode');

/******************************************
*			PLUGIN: DISPLAY POSTS
******************************************/
/**
 * Display Posts Output Filter
 * @see https://displayposts.com/docs/the-output-filter/
 *
 */
function notam03_dps_output_customization( $output, $original_atts, $image, $title, $date, $excerpt, $inner_wrapper, $content, $class, $author, $category_display_text ) {

	$image = '<a class="" id="no-decoration" href="' . get_the_permalink() . '">';
	$image .= get_the_post_thumbnail();
	$image .= '</a>';

	$title = '<a class="dps-title entry-title" id="no-decoration" href="' . get_the_permalink() . '">';
	$title .= get_the_title();
	$title .= '</a>';

	if ($date) {
		$date = '<a class="" id="no-decoration" href="' . get_the_permalink() . '">' . '<span class="dps-post-date entry-meta">' . get_the_date() . '</span>' . '</a>';
	}

	if ($excerpt) {
		$excerpt = '<p class="dps-content">' . get_the_excerpt() . $content . '</p>';
	}

	$output = '<' . $inner_wrapper . ' class="' . implode( ' ', $class ) . '">';

	$output .= '<h3 class="">' . $title . '</h3>';
	$output .= $date;
	$output .= $image;
	$output .= $author . $category_display_text;
	$output .= $excerpt;
	$output .= '</' . $inner_wrapper . '>';

	return $output;
}
add_filter( 'display_posts_shortcode_output', 'notam03_dps_output_customization', 10, 11 );

/**
 * Set Defaults in Display Posts Shortcode
 * @see https://displayposts.com/2019/01/04/change-default-attributes/
 *
 * @param array $out, the output array of shortcode attributes (after user-defined and defaults have been combined)
 * @param array $pairs, the supported attributes and their defaults
 * @param array $atts, the user defined shortcode attributes
 * @return array $out, modified output
 */
function notam03_dps_defaults( $out, $pairs, $atts ) {
	$new_defaults = array(
		'wrapper' => 'div',
		'image_size' => 'large',
		'posts_per_page' => 20,
		'include_excerpt' => true,
		'include_date' => true
	);

	foreach( $new_defaults as $name => $default ) {
		if( array_key_exists( $name, $atts ) )
			$out[$name] = $atts[$name];
		else
			$out[$name] = $default;
	}

	return $out;
}
add_filter( 'shortcode_atts_display-posts', 'notam03_dps_defaults', 11, 3 );

/**
 * Display Posts - List upcoming events from The Events Calendar
 * @see https://displayposts.com/2019/01/04/display-upcoming-events-from-the-events-calendar/
 */
function notam03_dps_future_events( $args, $atts ) {

  // Only run on event queries
  if( 'tribe_events' != $args['post_type'] )
	return $args;

   $args['order'] = 'ASC';
   $args['orderby'] = 'meta_value';
   $args['meta_key'] = '_EventStartDate';
   $args['meta_type'] = 'DATETIME';

   $args['meta_query'] = array(
	  array(
		'key' => '_EventStartDate',
		'value' => date( 'Y-d-m H:i:s', current_time( 'timestamp' ) ),
		'compare' => '>'
	) );

	 return $args;
}
add_filter( 'display_posts_shortcode_args', 'notam03_dps_future_events', 12, 2 );

/**
 * Template Parts / Layouts with Display Posts Shortcode
 *
 * @see https://www.billerickson.net/template-parts-with-display-posts-shortcode
 *
 */
function notam03_dps_template_part( $output, $original_atts ) {
	// Return early if our "layout" attribute is not specified
	if( empty( $original_atts['layout'] ) )
		return $output;
	ob_start();

	// Wrap the output in a grid, laying the content out depending on the layout param
	echo '<div class="display-posts-listing-grid-' . $original_atts['layout'] . '">';
	get_template_part( 'template-parts/dps', $original_atts['layout'] );
	echo '</div><!--.display-posts-listing-grid-' . $original_atts['layout'] . '-->';

	$new_output = ob_get_clean();
	if( !empty( $new_output ) )
		$output = $new_output;
	return $output;
}
add_action( 'display_posts_shortcode_output', 'notam03_dps_template_part', 13, 2 );


/******************************************
*			PLUGIN: Ajax Search
******************************************/

// Check if ajax search lite is active, if not, just use normal search
function notam03_get_search()
{
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

	if ( is_plugin_active( 'ajax-search-lite/ajax-search-lite.php' ) ):
		echo do_shortcode('[wpdreams_ajaxsearchlite]');
	else:
		get_search_form(); // standard wp search
	endif;

};

/******************************************
*			SETUP
******************************************/

if (! function_exists('notam03_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function notam03_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on notam03, use a find and replace
         * to change 'notam03' to the name of your theme in all the template files.
         */
        load_theme_textdomain('notam03', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'notam03'),
        ));

/*         register_nav_menus(array( */
/*             'menu-2' => esc_html__('Secondary', 'notam03'), */
/*         )); */

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        /* add_theme_support('custom-background', apply_filters('notam03_custom_background_args', array( */
        /*     'default-color' => 'ffffff', */
        /*     'default-image' => '', */
        /* ))); */

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height'      => 250,
            'width'       => 250,
            'flex-width'  => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'notam03_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function notam03_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('notam03_content_width', 640);
}
add_action('after_setup_theme', 'notam03_content_width', 0);

/**
 * Enqueue scripts and styles.
 */
function notam03_scripts()
{
    wp_enqueue_style('notam03-style', get_stylesheet_uri());


    // wp_enqueue_script('notam03-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), '20151215', true);

    /*
    Include minified scripts
     */

    wp_enqueue_script('notam03-footerscripts', get_template_directory_uri() . '/js/footer.js', array('jquery'), '20151215', true);
    wp_enqueue_script('notam03-headerscripts', get_template_directory_uri() . '/js/header.js', array('jquery'), '20151215', false);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'notam03_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}



// This is a hack to make old images from the prosjekter post type work as normal featured images
// From: https://support.advancedcustomfields.com/forums/topic/set-featured-image-from-acf-image-field/
function acf_set_featured_image( $value, $post_id, $field   ){

	if(empty($value) || empty(get_post_type($post_id)))
		return $value;

	update_post_meta($post_id, '_thumbnail_id', $value);

	return $value;

};

// acf/update_value/name={$field_name} - filter for a specific field based on it's name
add_filter('acf/update_value/name=thumbnail', 'acf_set_featured_image', 10, 3);
