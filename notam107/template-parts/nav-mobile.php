<?php

/**
 * Template part for mobile navigation
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package notam03
 */

?>

<!-- HAMBURGER MENU -->
<nav id="site-navigation" class="main-navigation">

    <button class="hamburger hamburger--3dy" aria-controls="primary-menu" aria-expanded="false">
        <span class="hamburger-box">
            <span class="hamburger-inner"></span>
        </span>
    </button>
        <?php

            wp_nav_menu(array(
                'theme_location' => 'menu-1',
                'menu_id'        => 'mobile-menu',
                'menu_class' => 'mobile-menu-container'
            ));
            
        ?>
</nav><!-- main-navigation -->
