# Notam03

![screen](assets/avatar.png)

This is the 2018 wordpress theme update to [Notam02.no](Notam02.no)

Code by [Mads Kjeldgaard](https://madskjeldgaard.dk)

### FEATURES

-   Based on the [underscores](https://underscores.me/) starter theme
-   Uses Gulp as a task manager to clean up the files and create a distribution ready theme in the _dist_ subfolder
-   Nicely organized Sass code which is compiled and minified by Gulp.
-   Customized support for Ajax Search
-   Customized support for The Events Calendar
-   Customized support for Display Posts

### Installation

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

### Development

prerequisites: [gulp](http://gulpjs.com/), [npm](http://npmjs.com/)

1. In a terminal, go to this folder, run `npm install` to download the necessary node packages for gulp
2. In the terminal, run `gulp watch` while editing the code (you need to set up the local host properly in the `gulpfile.js`)
3. To build, run `gulp build` which will minify and copy all the files to the _dist_ subfolder and remove node_modules and other things unnecesary for production.

### Credits

-   Based on Underscores https://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
-   normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
-   normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)
