<?php
/**
 * Template part for displaying custom post type prosjekt
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package notam03
 */

?>

<?php
/* Legacy stuff from old notam theme that used a lot of custom fields */
$thumbnail = '';
$body = '';

/**
 * Detect plugin. For use on Front End only.
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if ( is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ):

    $thumbnail = get_field('thumbnail');

endif;
?>

<div class="entry-header-item">
         <header class="entry-header ">
	    <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
         </header><!-- .entry-header -->
     </div><!-- .entry-header-item -->

    <div class="entry-content-item">
         <div class="entry-content ">
	    <!-- TODO: GRIDS -->
	   <div class="prosjekt-top-grid">

		<!-- Thumbnail -->
		<?php if (!$thumbnail): ?>
		<?php notam03_post_thumbnail(); ?>
		<?php elseif ($thumbnail): ?>
			<div class="prosjekt-thumbnail-item">
				<img src="<?php echo $thumbnail['url'] ?>" alt="">
			</div>
		<?php endif; ?>

		<div class="prosjekt-detail-item prosjekt-meta">
		    <!-- Details -->
		    <?php if( have_rows('details') ): ?>

			<?php while ( have_rows('details') ) : the_row(); ?>
				<div class="prosjekt-details smalltext prosjekt-<?php the_sub_field('name'); ?>">
					<b><?php the_sub_field('name'); ?>: </b>
					<span><?php the_sub_field('body'); ?></span>
				</div>

			<?php endwhile; ?>

		    <?php endif; ?>
		</div> <!-- .prosjekt-detail-item -->

		<!-- Description -->
		<div class="prosjekt-description-item"><h3 class="entry-title"><?php the_field('description'); ?></h3></div>


	    <!-- Content -->
	    <div class="prosjekt-body-item"><?php the_field('body'); ?></div>

	    <!-- Extra innhold-->
	    <div class="prosjekt-extra-item">
	    <?php if( have_rows('extra_innhold') ): ?>
		<b>Links:</b>
		<?php while ( have_rows('extra_innhold') ) : the_row(); ?>
		    <div class="prosjekt-details prosjekt-<?php the_sub_field('name'); ?>">
			    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('name'); ?></a>
		    </div>
		<?php endwhile; ?>

	    <?php endif; ?>

	    </div>

	    <!-- Gallery -->
	    <div class="prosjekt-gallery-item">
	    <?php if( have_rows('gallery') ): ?>
		<?php while ( have_rows('gallery') ) : the_row(); ?>

		    <?php $gallery_img = get_sub_field('image'); ?>
		    <img class="prosjekt-gallery-image" src="<?php echo $gallery_img['url']; ?>" alt="<?php echo $gallery_img['alt'] ?>" />

		<?php endwhile; ?>

	    <?php endif; ?>
	    </div>

		</div> <!-- .prosjekt-top-grid -->
	    </div>
	</div><!-- .entry-content -->
     </div><!-- entry-content-item -->


        <div class="entry-content-footer">
             <footer class="entry-footer ">
                 <?php /*the_post_navigation(); */?>
                 <?php /*notam03_entry_footer();*/ ?>
             </footer><!-- .entry-footer -->
       </div><!-- .entry-content-footer -->


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
</article><!-- #post-<?php the_ID(); ?> -->
