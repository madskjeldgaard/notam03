<?php

/**
 * Template part for displaying large posts using the Display Posts plugin
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package notam03
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="entry-header-item">
         <header class="entry-header ">

			<div class="entry-meta entry-meta-item dps-list-item">
				<!-- Post date -->
				<a id="no-decoration-dark" class="dark-link" href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
					<?php echo notam03_posted_on() . ':'; /* notam03_posted_by(); */ ?>
				</a>

				<!-- Post title -->
				<span class="dps-list-layout-title">
					<a id="no-decoration-dark" class="dark-link" href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
						<?php echo the_title(); /* notam03_posted_by(); */ ?>
					</a>
				</span>

			</div><!-- .entry-meta  -->

         </header><!-- .entry-header -->
     </div><!-- .entry-header-item -->

</article><!-- #post-<?php the_ID(); ?> -->
