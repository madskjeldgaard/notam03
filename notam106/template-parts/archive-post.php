<?php
/**
 * The template for displaying archive pages for regular post types
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package notam03
 */
?>
<?php
/* Legacy stuff from old notam theme that used a lot of custom fields */
$thumbnail = '';
$body = '';

/**
 * Detect plugin. For use on Front End only.
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if ( is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ):

    $thumbnail = get_field('thumbnail');

endif;
?>
    <div class="page-content-item archival-post">
<div class="entry-header-item">

         <header class="entry-header ">
	    <a id="no-decoration" href="<?php the_permalink(); ?>">

		<div class="entry-meta ">
		    <?php echo notam03_posted_on(); ?>
		</div><!-- .entry-meta  -->

		<h4 class="entry-title"><?php the_title(); ?></h4>
	    </a>
         </header>

</div><!-- entry-header-item -->

	<!-- Thumbnail -->
	<?php if (!$thumbnail): ?>
	    <?php notam03_post_thumbnail(); ?>
	<?php elseif ($thumbnail): ?>
	    <div class="thumbnail new-thumb">
		<img src="<?php echo $thumbnail['url'] ?>" alt="">
	    </div>
	<?php endif; ?>

<div class="entry-content-footer"></div><!-- entry-content-footer -->
<div class="entry-comment-item"></div><!-- entry-comment-item -->
    </div><!-- .page-content-item -post -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
</article><!-- #post-<?php the_ID(); ?> -->
