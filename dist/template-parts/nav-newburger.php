<?php

/**
 * Template part for all navigation
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package notam03
 */

?>
      
<!-- MAIN NAVIGATION -->
<nav id="main-navigation" class="main-navigation">

	<input type="checkbox" class="toggler">

	<div class="theburger"><div></div></div>

	<div class="burgermenu">
		<div>
			<div>
				<?php
				wp_nav_menu(array(
					'theme_location' => 'menu-1',
					'echo' => 'false'
					/* 'menu_id'        => 'primary-menu', */
					/* 'container_id' => 'desktop-main-menu' */
					));
				?>
			</div>
		</div>
	</div><!-- .burgermenu -->

</nav><!-- #main-navigation .main-navigation -->
