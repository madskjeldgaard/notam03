<?php

/**
 * Template part for the submenu (only used in desktop views)
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package notam03
 */

?>
     
<!-- SUB NAVIGATION -->
<nav id="sub-navigation" class="main-navigation">
    <?php
    /* wp_nav_menu(array( */
    /*     'theme_location' => 'menu-2', */
    /*     'menu_id'        => 'secondary-menu', */
    /* )); */
    ?>
</nav><!-- #sub-navigation .main-navigation -->
