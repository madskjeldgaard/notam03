<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package notam03
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		    <?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) :?>
			    <?php the_post(); ?>

			    <?php
			    /*
			     * Include the Post-Type-specific template for the content.
			     * If you want to override this in a child theme, then include a file
			     * called content-___.php (where ___ is the Post Type name) and that will be used instead.
			    */
			    ?>

			    <?php if (get_post_type() == 'prosjekter'):?>

				<div class="page-grid-slim">
				    <?php get_template_part( 'template-parts/archive', 'prosjekter'); ?>
				</div><!-- .page-grid-slim -->

			    <?php else:?>

				<div class="page-grid-slim">
				    <?php get_template_part( 'template-parts/archive', 'post'); ?>
				</div><!-- .page-grid-slim -->

			    <?php endif; ?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

		    <?php else : ?>
			<?php get_template_part( 'template-parts/content', 'none' );?>
		    <?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
