<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package notam03
 */

?>


</div><!-- #content -->
    <footer id="colophon" class="site-footer">
        <div class="site-info">
        <div class="footer-grid">

            <div class="footer-fw1">
                <?php dynamic_sidebar('footer-widgets1'); ?>
            </div>

            <div class="footer-fw2">
                <?php dynamic_sidebar('footer-widgets2'); ?>
            </div>

            <div class="footer-fw3">
                <?php dynamic_sidebar('footer-widgets3'); ?>
            </div>

            <div class="footer-fw4">
                <?php dynamic_sidebar('footer-widgets4'); ?>
            </div>

            <div class="footer-fw5">
                <?php dynamic_sidebar('footer-widgets5'); ?>
            </div>

        </div> <!-- .footer-grid -->
        </div><!-- .site-info -->
    </footer><!-- #colophon -->


<?php wp_footer(); ?>

</div> <!-- #page -->
</body>
</html>
