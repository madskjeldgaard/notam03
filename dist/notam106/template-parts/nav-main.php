<?php

/**
 * Template part for desktop navigation
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package notam03
 */

?>
     
<!-- MAIN NAVIGATION -->
<nav id="main-navigation" class="main-navigation">

    <button class="hamburger hamburger--3dy" aria-controls="primary-menu" aria-expanded="false">
        <span class="hamburger-box">
            <span class="hamburger-inner"></span>
        </span>
    </button>

    <?php
    wp_nav_menu(array(
        'theme_location' => 'menu-1',
        'menu_id'        => 'primary-menu',
        'container_id' => 'desktop-main-menu'
        ));
    ?>
</nav><!-- #main-navigation .main-navigation -->
