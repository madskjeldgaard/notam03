<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package notam03
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="page" class="site">

    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'notam03'); ?></a>

    <header id="masthead" class="site-header">
        <div class="mobilemenu"> 
            <?php //get_template_part( 'template-parts/nav', 'mobile'); ?>
        </div>

        <div class="menugrid-sub">
            <?php //get_template_part( 'template-parts/nav', 'sub'); ?>
        </div><!-- .menugrid-sub -->

        <div class="menu-grid">
            <div class="menugrid-branding site-branding">
                <div class="heartbeat-hover">
                    <?php if (has_custom_logo()) { the_custom_logo(); } ?>
                </div><!-- .heartbeat-hover -->
            </div><!-- menugrid-branding -->

            <div class="menugrid-main" id="navigation-border">
                <?php get_template_part( 'template-parts/nav', 'newburger'); ?>
            </div><!-- .menugrid-main -->

            <div id="header-search" class="menugrid-search">
                <?php 
                    notam03_get_search();
                ?>
            </div><!-- .menugrid-search -->
        </div><!-- .menu-grid -->
    </header>
<div id="content" class="site-content">
